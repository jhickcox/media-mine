## The Problem

This endeavor was inspired around Halloween of last year when I was searching online for Halloween-related movie and TV show suggestions. 
Perhaps overly encouraged by the volume of ‘Holiday top movie’ lists, I expected there to be an abundance of similar 
articles for what we all know is the superior holiday.
 
Surprisingly this was not the case, and any list I did find consisted predominantly of horror movies, ignoring 
(in my opinion) a rich vein of less scary and more thematically Halloween-y TV shows and movies.
Initially I just wanted to manually develop a good list of Halloween content, but I soon realized that if I wanted 
to surface novel content, simply compiling the existing lists of overlapping and movie-oriented lists would not suffice. Moreover, I wanted to find entire TV series that fit the theme (Scream Queens, anyone?) rather than individual episodes. I needed an automated method that would allow me to discover movies and shows based on their content rather than their inclusion in pre-existing lists.

## The Data
After some searching, I decided to use The Movie DB (TMDb) https://www.themoviedb.org/ API for my data source. 

TMDb had several advantages:
1. **It’s free.** Signing up for an account gave me access to all their movie and TV data.
2. **Reasonable rate limits.** I knew I might be hitting certain endpoints a lot (hundreds of times at most) and
 strict rate limits would make this difficult.
3. **Movies and TV shows.** Most aggregated lists online differentiate between movies and tv shows, but the way 
content is consumed and produced these days makes this distinction seem unnecessary to me. I wanted to treat TV show 
and movies the same way.
4. **Content keywords.** This might be the most important feature of this dataset for this project. Each movie or TV show object contains a list of keywords associated with its content. For example, Avengers: Infinity War contains the keywords magic, sacrifice, superhero, based on comic, and so on. More importantly, you can search for by keyword and receive a list of movies/shows that contain that keyword. So if I wanted to start with the keyword ‘Halloween’ I could easily search for items that contain it as a keyword.

##The Method

First I needed to explore the data. Since the TV shows and movies use different API endpoints, I focused first on 
gathering movies. 

I made a request for all movies with the keyword ‘halloween’. I filtered out any movies with a vote count of 0 or a 
voting average of less than 1, as well as any adult movies or movies not in English. As an aside: I know there are
tons of great movies that aren’t in English but I wanted to keep it simple for the sake of qualitative analysis later on. 
This request returned 164 movies. Not bad! But also a bit overwhelming already. I sorted by a handy ‘popularity’ 
core provided by the API. 

The top 10 movies sorted this way are:
1. Harry Potter and the Philosopher's Stone
2. Scary Stories to Tell in the Dark
3. E.T. the Extra-Terrestrial
4. Halloween (1984)
5. Halloween (2018)
6. Casper
7. The Nightmare Before Christmas
8. Goosebumps 2: Haunted Halloween
9. The Prodigy
10. Scream

The top 10 movies by voting average are:
1. Martha Stewart Holidays: Martha's Halloween Ideas
2. Universal Orlando's Horror Nights: The Art of the Scare
3. Underfist: Halloween Bash
4. Ed, Edd n Eddy's Boo Haw Haw
5. The Paul Lynde Halloween Special
6. The Halloween That Almost Wasn't
7. George's Island
8. The Shape
9. Halloween: The Inside Story
10. Harry Potter and the Philosopher's Stone

Clearly popularity is a better measure of a movie’s quality than the voting average. I am not sure how TMDb calculates
 popularity, but I assume it factors in both voting average and the total number of votes. The top movies by voting
  average are relatively lesser known and less relevant, so I feel good using popularity as a stand-in for general quality.

The top movies by popularity is a solid list of high-quality Halloween movies. But there’s a tradeoff between movies 
that are explicitly about Halloween and movies that are good. The popularity of movies on this list drops off 
precipitously after the top few, with a large cluster of unpopular movies at the bottom:

![alt text](/img/pop_scores.png "Popularity Scores for Movies Containing Keyword 'Halloween'")

Can I find more high-quality movies that don’t contain the keyword ‘halloween’ directly but that contain
 keywords that frequently co-occur with ‘halloween’?
 
#### Keyword Exploration

The 164 movies that contain the keyword ‘halloween’ also contain other keywords. I made a dictionary summing the 
number of occurrences of each keyword within these movies, ignoring the original keyword ‘halloween’ which we already
 know occurs in each one. Here’s a fun word cloud of the keyword frequencies, including the original keyword.

![alt text](/img/round_1_keyword_frequencies.png "Frequency of keywords cooccuring with 'Halloween'")


The 10 most common keywords are:
1. Slasher (occurs 17 times)
2. Murder (17)
3. Witch (12)
4. Haunted house (11)
5. Sequel (11)
6. Trick or treating (11)
7. Monster (11)
8. Ghost (10)
9. Serial killer (9)
10. Holiday (9)

Most of these keywords make sense--there are tags suggesting horror films and other Halloween-y items, but there 
are also tags that seem irrelevant (Sequel) or potentially misleading (Holiday). Interestingly, the most common 
keyword only appears in 17 of the movies, or about 10% of the original movies. This suggests that there are a 
variety of different Halloween movie categories and confirms my suspicion that Halloween movie lists that focus 
solely on slasher films miss out on lots of other content. So the most common keywords are generally on the right 
track but do not in themselves fully express the content of these movies.

Of the 536 keywords that describe these movies, 418 of them only occur one time. Sampling 10 of them randomly returns:
- Gangster
- Marijuana
- Minister
- Haunted
- Holiday season
- Pitchfork
- Cartoon cat
- Pact with the devil
- Brutality
- House

These tags span from pretty random (cartoon cat) to very specific (pact with the devil). I had been hoping that these
 keywords weren’t very Halloween-specific so I could discard them and significantly reduce the number of keywords I 
 had to work with, but after looking over them I decided I couldn’t simply throw them away. It appears that the 
 graph of halloween movies that I’m attempting to find is made up of many different sparse keywords which convey
 meaning when taken all together.

To expand my group of possible movies, I iterated through each keyword from my first group of movies and fetched all 
movies from the database containing that keyword. Rather than try to be clever about filtering movies while fetching 
them, I simply fetched all movies to filter on disk once I received it. 

If a keyword returned more than 600 movies, I deemed it too common and ignored the results. We are trying to find movies
 that are uniquely Halloween related--if there are too many movies associated with a keyword, it isn’t unique to 
 anything and won’t help us narrow down our results. This strategy filters out results for ‘based on a novel or book’, 
 ‘woman director’, and ‘zombie’ but allows ‘party’ and ‘supernatural’. I chose the cutoff number 600 after looking 
 through the number of movies returned for each keyword so the number is somewhat arbitrary, but it seems like a good 
 enough balance for now.

This round of fetching returned about 28,000 movies, orders of magnitude more than the previous round of movies. 
Too many movies! Let’s filter out movies that only contain a single relevant keyword. Doing this leaves us with 
9,388 movies. I end up filtering movies with 4 keywords or fewer, leaving 784 movies.

![alt text](/img/round_1_2_keyword_overlap.png "Frequency of keywords from round 1 appearing in round 2 movies")

Given that we have 18,914 movies that contain only one keyword from the first round, I was curious to see if there
 were certain keywords that appeared often as the ‘singleton’ keyword for a movie. A keyword appearing frequently as
 the sole connection to a movie indicates that it is not related to the other keywords and is only adding noise. Of
  the 450 keywords that appear as singletons in movies, the graph below displays the number of keywords over the number
   of movies they appear as singletons in. As you can see, many keywords only appear in a few movies as singletons, but there are others that appear hundreds of times.

![alt text](/img/singleton_frequency.png "How many times keywords appear as a movie's only keyword")

Looking at the top offenders and the number of movies they are the sole link to, we see:
1. Gangster - 364
2. Holiday - 335
3. Marriage - 319
4. Cartoon - 316
5. Remake - 305

These tags seem unrelated to Halloween, so let’s remove all the keywords that appear in more than 50 unrelated movies. After removing these 118 keywords and filtering movies that fewer than 4 keywords given these deletions, we are left with 85 movies. This sounds like a reasonable amount to return as a final list.

##The Ranking

Now that we have a final set of movies, let’s decide how to rank them by both relevance and quality.

Sorting the remaining movies by popularity yields the following top movies:
1. Harry Potter and the Philosopher's Stone (30.989)
2. Harry Potter and the Order of the Phoenix  (22.854)
3. Beetlejuice (22.493)
4. Harry Potter and the Goblet of Fire (22.051)
5. Scary Stories to Tell in the Dark (19.675)
6. E.T. the Extra-Terrestrial (17.812)
7. Casper (17.185)
8. Ghostbusters (16.171)
9. Halloween (2018) (16.053)
10. Scream 4 (15.298)

Comparing this list to the most popular movies from round 1, we see that we’ve added more Harry Potter movies and bumped up a couple high quality movies. 

Sorting them by how many relevant keywords they contain yields:
1. Hell House LLC (19)
2. E.T. the Extra-Terrestrial (15)
3. Halloween II (15)
4. Harry Potter and the Philosopher's Stone (11)
5. Halloween (11)

To combine these two metrics, I normalized both popularity and the number of keywords to be between [0, 1].
 I also added an indicator variable for whether the movie contains the original keyword.

Since there is no labeled data or scores to regress to, I chose my weights manually for each of these three 
features and fine tuned using qualitative analysis. Starting with equal weight for each feature (0.33 for 
each to keep things roughly normalized) returned this for the top 10:

| Title | Score | Popularity | Num Keywords | Has Original Keyword |
| --- | --- | --- | --- | --- |
| Harry Potter and the Philosopher's Stone | 0.801429  | 1.000000  | 0.428571 | 1.0 |
| E.T. the Extra-Terrestrial | 0.752623 | 0.566389 | 0.714286 | 1.0 |
| Hell House LLC | 0.723190 | 0.191484 | 1.000000 | 1.0 |
| Halloween II | 0.693147 | 0.386159 | 0.714286 | 1.0 |
| Halloween | 0.693147 | 0.442561 | 0.428571 | 1.0 |
| The Prodigy | 0.605670 | 0.406792 | 0.428571 | 1.0 |
| Casper | 0.604385 | 0.545757 | 0.285714 | 1.0 |
| Scary Stories to Tell in the Dark | 0.584282 | 0.627694 | 0.142857 | 1.0 |
| Halloween H20: 20 Years Later | 0.584201 | 0.341735 | 0.428571 | 1.0 |
| Scream | 0.577780 | 0.465135 | 0.285714 | 1.0 |

This ranking is not perfect. A couple things I notice right away: every movie in the top 50 contains the original keyword, and the 3rd ranked movie
 has a popularity of 0.191. There's a balance between returning the most popular movies in the list 


## Sub-Genre Clustering

Now that we have a group of Halloween related movies, let's see if we can group them into sub-genres.

Choosing an arbitrary number of clusters `k=4`, I ran K-means clustering on the filtered movies, ignoring the
singleton filtering because I wanted to work with a greater number of movies in this case. By representing
each movie as a vector of its keywords, I can calculate the Euclidean distance between movies, clustering movies together
 that share keywords. The resulting clusters are not evenly-sized: they contain 577, 29, 77, and 12 movies respectively.
 Increasing k did not do much to more evenly distribute movies between clusters, nor did initializing to new random
 centroids.
 
 Word clouds representing keyword frequencies in each cluster are as follows:
 
![alt text](/img/cluster_keyword_frequencies.png "Keyword Frequencies for each Cluster")

The most populated cluster is the upper left cloud. You can see that there are two horror/slasher clusters, the
difference between them being that the first one involves more supernatural words and the second one more human
threats. The bottom left cluster seems to have captured most of the movies with the original 'Halloween' keyword,
suggesting that the clustering might have captured concetric circles of relevance to Halloween. Finally, the last and
least populated cluster contains the least relevant movies in that they are the first to be filtered out when we
filter by minimum keyword number and singletons. These movies are campy but still Halloween related, and it's fun to 
see that our clustering detected that sub-genre.

Next up: Rank by graph analysis and sub-topic clustering
